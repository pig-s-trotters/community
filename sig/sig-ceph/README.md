
# sig-ceph 愿景

愿景：汇聚存储英才，共同打造高质量、高性能、高可靠性的ceph版本，构建丰富的南北向生态

# 目标

- 打造两层质量防护网，确保ceph在各个版本上高质量发布
- 及时解决和处理ceph相关的issue、pr和CVE，确保ceph的高质量按时发布
- 针对ARM架构开展优化，打造ceph极致性能
- 紧跟ceph原生社区发展，坚持“upstream first”原则，丰富openeuler ceph生态圈
- 根据用户诉求和ceph sig组成员共同决策openeuler新版本上ceph的版本


# 组织会议

北京时间，双周一晚上，20点~21点


# 成员

### Maintainer列表

- chixinze[@chixinze](https://gitee.com/chixinze)
- liuzhiqiang[@liuzhiqiang26](https://gitee.com/liuzhiqiang26)
- liuqinfei[@liuqinfei](https://gitee.com/liuqinfei) (负责Ceph CI工作)

### Committer列表

- sunligang[@nick-slg-kylin](https://gitee.com/nick-slg-kylin)
- luorixin[@rosinL](https://gitee.com/rosinL)
- Zhiwei-Dai[@Zhiwei-Dai](https://gitee.com/Zhiwei-Dai)
- wangxiaomeng[@tjwangxm](https://gitee.com/tjwangxm)
- mayilin[@kymayl](https://gitee.com/kymayl)
- chenjian[@yahoohey](https://gitee.com/yahoohey)


### 模块责任人

- MON:     mayilin,chenjian
- OSD:     luorixin
- RGW:     daizhiwei
- MDS:     sunligang,wangxiaomeng
- RADOS:   chixinze
- RBD:     daizhiwei

# 联系方式


- 邮件列表 <dev@openeuler.org>



# 项目清单


项目名称：

repository地址：

- https://gitee.com/src-openeuler/ceph
- https://gitee.com/src-openeuler/ceph-deploy
- https://gitee.com/src-openeuler/ceph-ansible
- https://gitee.com/src-openeuler/ceph-csi
- https://gitee.com/src-openeuler/teuthology
