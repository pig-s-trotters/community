
# sig-AccLib

OpenEuler AccLib(加速库)项目团队致力于提供硬件加速引擎和软件加速库，为大数据加解密、分布式存储压缩、视频转码等应用场景提供高性能加速。

### 工作目标

 - 在OpenEuler社区添加对加速库的支持
 - 为OpenEuler平台的上层应用软件提供性能加速服务
 - 丰富OpenEuler生态圈，提供多样性算力

### 工作范围

 - AccLib在OpenEuler的使能、维护、升级等
 - AccLib安装包的构建、更新，并提供构建的脚本和使用文档等
 - AccLib SIG组所有相关的文档、会议、邮件列表、IRC的管理


# 组织会议

- （暂定）公开的会议时间：北京时间，双周三下午，16点~17点

*<可选，请在此给出SIG会议的时间，如SIG的公开会议时间还未确定，可以放到确定以后刷新>*



# 成员


### Maintainer列表

- xuqimeng[@xqm227](http://gitee.com/xqm227)
- yuhongxiao[@wuliaokanke](https://gitee.com/wuliaokanke)
- zhangxuelei[@derekpush](https://gitee.com/derekpush)
- zhongkeyi[@realzhongkeyi](https://gitee.com/realzhongkeyi)
- fanghao[@hao-fang](https://gitee.com/hao-fang)


### Committer列表

- xuqimeng[@xqm227](https://gitee.com/xqm227)
- yuhongxiao[@wuliaokanke](https://gitee.com/wuliaokanke)
- zhangxuelei[@derekpush](https://gitee.com/derekpush)
- zhongkeyi[@realzhongkeyi](https://gitee.com/realzhongkeyi)
- fanghao[@hao-fang](https://gitee.com/hao-fang)


# 联系方式

dev@openeuler.org


# 项目清单

项目名称：
AvxToNeon

repository地址：

- https://gitee.com/openeuler/AvxToNeon
- https://gitee.com/openeuler/libkae
- https://gitee.com/openeuler/libwd
- https://gitee.com/openeuler/kae_driver
- https://gitee.com/src-openeuler/libkae
- https://gitee.com/src-openeuler/libwd
- https://gitee.com/src-openeuler/kae_driver
- https://gitee.com/src-openeuler/uadk_engine
